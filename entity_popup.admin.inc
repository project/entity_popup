<?php
/**
 * @file
 * Administration page callbacks for the Entity Popup module.
 */

/**
 * Form callback for the Entity Popup settings.
 */
function entity_popup_admin_settings() {
  $form['entity_popup_start_disabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('All entity popup settings start disabled.'),
    '#default_value' => variable_get('entity_popup_start_disabled', FALSE),
    '#description' => t('If checked, all entity popups are disabled until intentionally configured otherwise.'),
  );
  return system_settings_form($form);
}
